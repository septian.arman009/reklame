<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-sign-out"></span> Log
				<strong>Out</strong> ?</div>
			<div class="mb-content">
				<p>Anda yakin ingin keluar ?</p>
				<p>Klik No untuk tetap bekerja, klik YES untuk keluar.</p>
			</div>
			<div class="mb-footer">
				<div class="pull-right">
					<a class="btn btn-success btn-lg" onclick="logout()">Yes</a>
					<button class="btn btn-default btn-lg mb-control-close">No</button>
				</div>
			</div>
		</div>
	</div>
</div>

<button id="btn-c" style="display:none" class="btn btn-default mb-control" data-box="#d_confirm"></button>
<div class="message-box message-box-warning animated fadeIn" id="d_confirm">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-warning"></span> Confirmation</div>
			<div class="mb-content">
				<p id="message-btn-c"></p>
			</div>
			<div class="mb-footer">
				<a id="yes-btn-c" class="btn btn-success btn-lg mb-control-close">Yes</a>
				<button onclick="resetModal()" class="btn btn-default btn-lg mb-control-close">No</button>
			</div>
		</div>
	</div>
</div>

<button id="btn-s" style="display:none" class="btn btn-default mb-control" data-box="#message-box-success"></button>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-check"></span> Success </div>
			<div class="mb-content">
				<p id="message-btn-s"></p>
			</div>
			<div class="mb-footer">
				<button onclick="resetModal()" class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>

<button id="btn-e" style="display:none" class="btn btn-default mb-control" data-box="#message-box-warning"></button>
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="message-box-warning">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-times"></span> Error </div>
			<div class="mb-content">
				<p id="message-btn-e"></p>
			</div>
			<div class="mb-footer">
				<button onclick="resetModal()" class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- END MESSAGE BOX-->