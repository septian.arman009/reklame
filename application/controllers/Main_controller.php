<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model', 'mm');
        auth();
    }

    public function index()
    {
        $this->load->view('content/admin/main');
    }

    public function home($page)
    {
        if(role(['admin'], false)){
            $perPage = 10;
            $total_row = $this->mm->countRow('logs');
            $data['all'] = ceil($total_row / 10);
            if ($page != 1) {
    
                $end = $perPage * $page;
                $start = $end - 10;
                $data['page'] = $page;
    
                if ($start > 0) {
                    $data['logs'] = $this->mm->getArrayLimitOrder('logs', $start, $end, 'created_at', 'desc');
                    $this->load->view('content/admin/home/home', $data);
                } else {
                    $data['logs'] = '';
                    $this->load->view('content/admin/home/home', $data);
                }
    
            } else {
                $data['page'] = $page;
                $data['logs'] = $this->mm->getArrayLimitOrder('logs', 0, $perPage, 'created_at', 'desc');
                $this->load->view('content/admin/home/home', $data);
            }
        }
    }

    public function destroy($table, $column)
    {
        $obj = to_json();
        $id = $obj->id;

        if ($table == 'users' && $id == whoIAM()['id']) {
            r_error();
        } else if (role(['admin'], false)) {
            if($table == 'users'){
                $email = $this->mm->getOne('users', 'email', 'user_id', $id);
                logs("Menghapus user dengan email : {$email}");
            }else if($table == 'skpd'){
                $skpd = $this->mm->getArrayWhere('skpd', 'skpd_id', $id);
                if($skpd[0]['pdf_file']){
                    unlink("./assets/uploads/files/" . $skpd[0]['pdf_file']);
                }
                if($skpd[0]['billboard_img']){
                    unlink("./assets/uploads/images/" . $skpd[0]['billboard_img']);
                }
                logs("Menghapus SKPD dengan nomor : {$skpd[0]['skpd_number']}");
            }else if($table == 'atm'){
                
                $name = $this->mm->getOne('atm', 'name', 'atm_id', $id);
                $skpd = $this->mm->getArrayWhere('skpd', 'atm_id', $id);
                foreach ($skpd as $key => $value) {
                    if($value['pdf_file'] != ''){
                        if(file_exists("./assets/uploads/files/" . $value['pdf_file'])){
                            unlink("./assets/uploads/files/" . $value['pdf_file']);
                        }
                    }
                    if($value['billboard_img'] != ''){
                        if(file_exists("./assets/uploads/images/" . $value['billboard_img'])){
                            unlink("./assets/uploads/images/" . $value['billboard_img']);
                        }
                    }
                }
                $this->mm->destroy('skpd', 'atm_id', $id);
                logs("Menghapus ATM dengan ID : {$name}");
            }
        
            $this->mm->destroy($table, $column, $id);
            r_success();

        }
    }

    public function emailSystem()
    {
        if(role(['admin'], false)){
            $data['setting'] = $this->mm->getArrayWhere('settings', 'setting_id', 1);
            $this->load->view('content/admin/settings/emailSystem', $data);
        }
    }

    public function updateEmailSystem()
    {
        $obj = to_json();
        $data['protocol'] = $obj->protocol;
        $data['mail_host'] = $obj->mail_host;
        $data['mail_port'] = $obj->mail_port;
        $data['send_mail'] = $obj->send_mail;
        $data['send_pass'] = $obj->send_pass;
    
        $update = $this->mm->update('settings', $data, 'setting_id', 1);
        if ($update) {
            logs("Update system email : {$data['send_mail']}");
            r_success();
        }
    }

    public function changePassword()
    {
        if(role(['admin'], false)){
            $this->load->view('content/admin/settings/changePassword');
        }
    }

    public function checkOldPassword()
    {
        $obj = to_json();
        $password = md5($obj->old_password);
        $check = $this->mm->getArray2Where('users', 'password', $password, 'user_id', whoIAM()['id']);
        if ($check) {
            r_success();
        }
    }

    public function updatePassword()
    {
        $obj = to_json();
        $data['password'] = md5($obj->password);
        $update = $this->mm->update('users', $data, 'user_id', whoIAM()['id']);
        if ($update) {
            logs("Change password");
            r_success();
        }
    }

    public function singleCheck()
    {
        $obj = to_json();
        $key_id = $obj->key_id;
        $value = $obj->value;
        $where = $obj->column;
        $column = $obj->column;
        $table = $obj->table;

        if ($key_id == 'null') {
            $check = $this->mm->getArrayWhere($table, $where, $value);
            if (!$check) {
                r_success();
            }
        } else {
            $old = $this->mm->getOne($table, $where, $column, $key_id);
            if ($value == $old) {
                r_success();
            } else {
                $check = $this->mm->getArrayWhere($table, $where, $value);
                if (!$check) {
                    r_success();
                }
            }
        }
    }

    public function tripleCheck()
    {
        $obj = to_json();
        $key_id = $obj->key_id;
        $value = $obj->value;
        $where = $obj->where;
        $column = $obj->column;
        $table = $obj->table;
        $table1 = $obj->table1;
        $table2 = $obj->table2;

        if ($key_id == 'null') {
            $check = $this->mm->getArrayWhere($table, $where, $value);
            $check1 = $this->mm->getArrayWhere($table1, $where, $value);
            $check2 = $this->mm->getArrayWhere($table2, $where, $value);
            if (!$check && !$check1 && !$check2) {
                r_success();
            }
        } else {
            $old = $this->mm->getOne($table, $where, $column, $key_id);
            if ($value == $old) {
                r_success();
            } else {
                $check = $this->mm->getArrayWhere($table, $where, $value);
                $check1 = $this->mm->getArrayWhere($table1, $where, $value);
                $check2 = $this->mm->getArrayWhere($table2, $where, $value);
                if (!$check && !$check1 && !$check2) {
                    r_success();
                }
            }
        }
    }

    public function getData()
    {
        $obj = to_json();
        $key_id = $obj->key_id;
        $where = $obj->where;
        $table = $obj->table;
        $data = $this->mm->getArrayWhere($table, $where, $key_id);
        if ($data) {
            r_success_data($data);
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('rek_in');
    }

}
