<ul class="breadcrumb">
	<li>
		<a href="#">Reklame</a>
	</li>
	<li class="active">ATM</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar ATM</h3>
				</div>
				<div class="panel-body">
					<button onclick="loadView('reklame_controller/form/atm/null/null', '.content')"
						class="btn btn-default mb-control" data-box="#message-box-sound-2">Tambah ATM</button>
					<br>
					<br>
					<table id="atm-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">ATM ID</th>
								<th id="th">Lokasi Pemasangan</th>
								<th id="th">Jatuh Tempo</th>
								<th id="th" class="no-sort" width="10%">Status</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">ATM ID</th>
								<th class="footer">Lokasi Pemasangan</th>
								<th class="footer">DD-MM-YYYY</th>
								<th class="footer">status</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>

<script id="atmjs">
	$(document).ready(function () {
		$("#atm_spin").hide();
		$("#atm_normal").show();
	});

	$('#atm-table tfoot th').each(function () {
		var title = $(this).text();
		if (title == 'DD-MM-YYYY') {
			var inp =
				'<select class="form-control footer-s" id="' + title + '">' +
					'<option value=""> -Pilih Bulan- </option>' +
					'<option value="1"> Januari </option>' +
					'<option value="2"> Februari </option>' +
					'<option value="3"> Maret </option>' +
					'<option value="4"> April </option>' +
					'<option value="5"> Mei </option>' +
					'<option value="6"> Juni </option>' +
					'<option value="7"> Juli </option>' +
					'<option value="8"> Agustus </option>' +
					'<option value="9"> September </option>' +
					'<option value="10"> Oktober </option>' +
					'<option value="11"> November </option>' +
					'<option value="12"> Desember </option>' +
				'</select>';
		} else {
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" id="' + title +
				'" />';
		}
		$(this).html(inp);
	});

	var table = $('#atm-table').DataTable({
		"processing": true,
		"serverSide": true,
		"order": [ [0, 'desc'] ],
		"ajax": {
			"url": 'reklame_controller/atmTable',
			"type": "POST"
		}
	});

	table.columns().every(function () {
		var that = this;
		$('input', this.footer()).on('keyup change', function () {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		});
	});

	table.columns().every(function () {
		var that = this;
		$('#DD-MM-YYYY', this.footer()).on('change', function () {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		});
	});

	$("#no").hide();
	$("#status").hide();

	function detailAtm(atm_id) {
		loadView('reklame_controller/detailAtm/' + atm_id, '.content');
	}

	document.getElementById('atmjs').innerHTML = "";
</script>

<style>
	#atm-table_filter {
		display: none;
	}
</style>