<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model', 'mm');
        auth();
    }

    public function form($form, $key_id, $param)
    {
        $data['key_id'] = $key_id;
        if ($form == 'users') {
            $data['roles'] = $this->mm->getArray('roles');
            $this->load->view('content/admin/master/user/form', $data);
        }
    }

    public function user()
    {
        if (role(['admin'], false)) {
            $this->load->view('content/admin/master/user/index');
        }
    }

    public function userTable()
    {
        $id1 = 'a.role_id';
        $id2 = 'b.role_id';
        $table1 = 'users a';
        $table2 = 'roles b';
        $column = array(
            'a.user_id',
            'a.name',
            'a.email',
            'b.display_name',
        );
        $original = array(
            'user_id',
            'name',
            'email',
            'display_name',
        );
        showTwoTable($id1, $table1, $id2, $table2, $column, $original, 'null');
    }

    public function userAction()
    {
        $obj = to_json();
        $key_id = $obj->key_id;
        $table = $obj->table;
        $data['name'] = ucwords($obj->name);
        $data['email'] = $obj->email;
        $data['role_id'] = $obj->role_id;
        if ($key_id == 'null') {
            $data['password'] = md5($obj->password);
            $data['created_at'] = date('Y-m-d H:i:s');
            $log_message = "Menambahkan user baru dengan email : {$data['email']}";
            do_action($key_id, $table, 'user_id', $data, $log_message);
        } else {
            $log_message = "Update user dengan email : {$data['email']}";
            do_action($key_id, $table, 'user_id', $data, $log_message);
        }
    }
}
