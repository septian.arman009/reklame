var modalError = $('#btn-e');
var modalSuccess = $('#btn-s');


function mError(message){
	modalError.click();
	$("#message-btn-e").html(message);
}

function mSuccess(message){
	modalSuccess.click();
	$("#message-btn-s").html(message);
}

function resetModal(){
	$("#message-btn-e").html('');
	$("#message-btn-s").html('');
}

function signinProccess() {
	var data = {
		email: $("#email").val(),
		password: $("#password").val()
	};

	postData("auth_controller/signinProccess", data, function (err, response) {
		if (response) {
			if (response.status == "success") {
				window.location = "admin";
			} else {
				mError('Login gagal, silakan cek email & password');
			}
		}
	});
}

function emailFormat() {
	var email = $("#email").val();
	if (email != '') {
		if (ValidateEmail(email, "#send_token", "#invalid_email")) {
			getEmail();
		}
	}else{
		$("#invalid_email").hide();
	}
}

function getEmail() {

	var data = {
		key: $("#email").val(),
		where: 'email',
		table: 'users'
	}

	postData('main_controller/getData', data, function (err, response) {
		if (response) {
			var status = response.status;
			if (status == 'success') {
				$("#notexist").show();
				$("#send_token").attr('disabled', 'disabled');
			} else {
				$("#notexist").hide();
				$('#send_token').removeAttr('disabled');
			}
		} else {
			console.log('ini error : ', err);
		}
	});
}

function send_token() {
	$("#send_token").html('Loading ..');
	var data = {
		email: $("#email").val()
	};

	postData("auth_controller/sendToken", data, function (err, response) {
		if (response) {
			if (response.status == "success") {
				$("#send_token").html('Kirim Link');
				mSuccess('Link lupa password telah dikirim, silakan cek email untuk membuat password baru');
				email: $("#email").val('')
			} else {
				$("#send_token").html('Send Token');
				mError('Gagal mengirim link reset password');
			}
		}
	});
}