<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reklame_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model', 'mm');
        auth();
    }

    public function form($form, $key_id, $param)
    {
        $data['key_id'] = $key_id;
        if ($form == 'atm') {
            if ($key_id != 'null') {
                $data['skpd_id'] = $param;
            } else {
                $data['skpd_id'] = 'null';
            }
            $this->load->view('content/admin/reklame/atm/form', $data);
        } else if ($form == 'skpd') {
            $data['skpd_id'] = $param;
            $this->load->view('content/admin/reklame/atm/skpd/form', $data);
        }
    }

    public function atm()
    {
        if (role(['admin'], false)) {
            $this->load->view('content/admin/reklame/atm/index');
        }
    }

    public function atmTable()
    {
        $id1 = 'a.atm_id';
        $id2 = 'b.atm_id';
        $table1 = 'atm a';
        $table2 = 'skpd b';
        $column = array(
            'a.atm_id',
            'a.name',
            'b.place_installation',
            'b.end_period',
            'a.status',
        );
        $original = array(
            'atm_id',
            'name',
            'place_installation',
            'end_period',
            'status',
        );
        showTwoTable($id1, $table1, $id2, $table2, $column, $original, 'null');
    }

    public function action()
    {
        $id = $this->input->post('key_id');
        $skpd_id = $this->input->post('skpd_id');

        $atm_name = strtoupper($this->input->post('name'));
        $skpd_data['skpd_number'] = $this->input->post('skpd_number');
        $skpd_data['co_area'] = ucwords($this->input->post('co_area'));
        $skpd_data['billboard_text'] = ucwords($this->input->post('billboard_text'));
        $skpd_data['place_installation'] = ucwords($this->input->post('place_installation'));
        $skpd_data['start_period'] = to_date_mysql($this->input->post('start_period'));
        $skpd_data['end_period'] = to_date_mysql($this->input->post('end_period'));
        $skpd_data['comment'] = ucwords($this->input->post('comment'));

        $start = new DateTime($skpd_data['start_period']);
        $end = new DateTime($skpd_data['end_period']);

        if($end > $start){
            
            if (!empty($_FILES["file_pdf"]["name"])) {
                $config = array();
                $file_pdf = $_FILES["file_pdf"]["name"];
                $file_pdf_ext = pathinfo($file_pdf, PATHINFO_EXTENSION);
                $new_pdf_name = md5(uniqid(rand(), true)) . '.' . $file_pdf_ext;
                $config['upload_path'] = "./assets/uploads/files";
                $config['allowed_types'] = 'pdf';
                $config['file_name'] = $new_pdf_name;
                $this->load->library('upload', $config, 'file_upload');
                $this->file_upload->initialize($config);
            }
    
            if (!empty($_FILES["billboard_img"]["name"])) {
                $config = array();
                $billboard_img = $_FILES["billboard_img"]["name"];
                $billboard_img_ext = pathinfo($billboard_img, PATHINFO_EXTENSION);
                $new_billboard_img_name = md5(uniqid(rand(), true)) . '.' . $billboard_img_ext;
                $config['upload_path'] = "./assets/uploads/images";
                $config['allowed_types'] = 'jpg|png|pdf|JPG|JPEG';
                $config['file_name'] = $new_billboard_img_name;
                $this->load->library('upload', $config, 'img_upload');
                $this->img_upload->initialize($config);
            }
    
            if ($id == 'null') {
                if (!empty($file_pdf)) {
                    $upload1 = $this->file_upload->do_upload("file_pdf");
                    $pdf = $this->file_upload->data();
                    $skpd_data['pdf_file'] = $pdf['file_name'];
                }
    
                if (!empty($billboard_img)) {
                    $upload2 = $this->img_upload->do_upload("billboard_img");
                    $img = $this->img_upload->data();
                    $skpd_data['billboard_img'] = $img['file_name'];
                }
    
                $atm_data['name'] = $atm_name;
                $atm_data['status'] = 1;
                $atm_data['created_at'] = date('Y-m-d H:i:s');
                $insert_atm = $this->mm->insertGetId('atm', $atm_data);
                if ($insert_atm) {
                    $skpd_data['atm_id'] = $insert_atm;
                    $skpd_data['created_at'] = date('Y-m-d H:i:s');
                    $insert_skpd = $this->mm->insertGetId('skpd', $skpd_data);
                    if ($insert_skpd) {
                        $atm_update['skpd_active'] = $insert_skpd;
                        $update_atm = $this->mm->update('atm', $atm_update, 'atm_id', $insert_atm);
                        if ($update_atm) {
                            logs("Menambah ATM baru dengan ID : {$atm_name}");
                            r_success();
                        }
                    }
                }
            } else {
                $old_skpd = $this->mm->getArrayWhere('skpd', 'skpd_id', $skpd_id);
                if (!empty($file_pdf)) {
                    $upload1 = $this->file_upload->do_upload("file_pdf");
                    $pdf = $this->file_upload->data();
                    $skpd_data['pdf_file'] = $pdf['file_name'];
                    if ($old_skpd[0]['pdf_file']) {
                        if(file_exists("./assets/uploads/files/" . $old_skpd[0]['pdf_file'])){
                            unlink("./assets/uploads/files/" . $old_skpd[0]['pdf_file']);
                        }
                    }
                }
    
                if (!empty($billboard_img)) {
                    $upload2 = $this->img_upload->do_upload("billboard_img");
                    $img = $this->img_upload->data();
                    $skpd_data['billboard_img'] = $img['file_name'];
                    if ($old_skpd[0]['billboard_img']) {
                        if(file_exists("./assets/uploads/images/" . $old_skpd[0]['billboard_img'])){
                            unlink("./assets/uploads/images/" . $old_skpd[0]['billboard_img']);
                        }
                    }
                }
    
                $skpd_data['update_at'] = date('Y-m-d H:i:s');
                $skpd_update = $this->mm->update('skpd', $skpd_data, 'skpd_id', $skpd_id);
                if ($skpd_update) {
                    $atm_data['name'] = $atm_name;
                    $skpd_active = $this->mm->maxSkpdPeriod1($id);
                    if (!$skpd_active) {
                        $skpd_active = $this->mm->maxSkpdPeriod2($id);
                    }
                    $atm_data['skpd_active'] = $skpd_active;
                    $update_atm = $this->mm->update('atm', $atm_data, 'atm_id', $id);
                    if ($update_atm) {
                        logs("Update ATM dengan ID : {$atm_name}");
                        r_success();
                    }
                }
            }
        }else{
           r_error_data("Periode akhir harus lebih besar daripada periode awal");
        }

       
    }

    public function skpdAction()
    {
        $skpd_id = $this->input->post('skpd_id');
        $skpd_data['atm_id'] = $this->input->post('key_id');
        $skpd_data['skpd_number'] = $this->input->post('skpd_number');
        $skpd_data['co_area'] = ucwords($this->input->post('co_area'));
        $skpd_data['billboard_text'] = ucwords($this->input->post('billboard_text'));
        $skpd_data['place_installation'] = ucwords($this->input->post('place_installation'));
        $skpd_data['start_period'] = to_date_mysql($this->input->post('start_period'));
        $skpd_data['end_period'] = to_date_mysql($this->input->post('end_period'));
        $skpd_data['comment'] = ucwords($this->input->post('comment'));

        $start = new DateTime($skpd_data['start_period']);
        $end = new DateTime($skpd_data['end_period']);

        if($end > $start){
            if (!empty($_FILES["file_pdf"]["name"])) {
                $config = array();
                $file_pdf = $_FILES["file_pdf"]["name"];
                $file_pdf_ext = pathinfo($file_pdf, PATHINFO_EXTENSION);
                $new_pdf_name = md5(uniqid(rand(), true)) . '.' . $file_pdf_ext;
                $config['upload_path'] = "./assets/uploads/files";
                $config['allowed_types'] = 'pdf';
                $config['file_name'] = $new_pdf_name;
                $this->load->library('upload', $config, 'file_upload');
                $this->file_upload->initialize($config);
            }
    
            if (!empty($_FILES["billboard_img"]["name"])) {
                $config = array();
                $billboard_img = $_FILES["billboard_img"]["name"];
                $billboard_img_ext = pathinfo($billboard_img, PATHINFO_EXTENSION);
                $new_billboard_img_name = md5(uniqid(rand(), true)) . '.' . $billboard_img_ext;
                $config['upload_path'] = "./assets/uploads/images";
                $config['allowed_types'] = 'jpg|png|pdf|JPG|JPEG';
                $config['file_name'] = $new_billboard_img_name;
                $this->load->library('upload', $config, 'img_upload');
                $this->img_upload->initialize($config);
            }
    
            if ($skpd_id == 'null') {
                if (!empty($file_pdf)) {
                    $upload1 = $this->file_upload->do_upload("file_pdf");
                    $pdf = $this->file_upload->data();
                    $skpd_data['pdf_file'] = $pdf['file_name'];
                }
    
                if (!empty($billboard_img)) {
                    $upload2 = $this->img_upload->do_upload("billboard_img");
                    $img = $this->img_upload->data();
                    $skpd_data['billboard_img'] = $img['file_name'];
                }
                
                $skpd_data['created_at'] = date('Y-m-d H:i:s');
                $insert_skpd = $this->mm->insertGetId('skpd', $skpd_data);
                if ($insert_skpd) {
                    $skpd_active = $this->mm->maxSkpdPeriod1($skpd_data['atm_id']);
                    if (!$skpd_active) {
                        $skpd_active = $this->mm->maxSkpdPeriod2($skpd_data['atm_id']);
                    }
                    $atm_update['skpd_active'] = $skpd_active;
                    $update_atm = $this->mm->update('atm', $atm_update, 'atm_id', $skpd_data['atm_id']);
                    if ($update_atm) {
                        $atm_name = $this->mm->getOne('atm', 'name', 'atm_id', $skpd_data['atm_id']);
                        logs("Menambah SKPD baru dengan Nomor : {$skpd_data['skpd_number']} pada ATM : {$atm_name}");
                        r_success();
                    }
                }
            } else {
                $old_skpd = $this->mm->getArrayWhere('skpd', 'skpd_id', $skpd_id);
                if (!empty($file_pdf)) {
                    $upload1 = $this->file_upload->do_upload("file_pdf");
                    $pdf = $this->file_upload->data();
                    $skpd_data['pdf_file'] = $pdf['file_name'];
                    if ($old_skpd[0]['pdf_file']) {
                        if(file_exists("./assets/uploads/files/" . $old_skpd[0]['pdf_file'])){
                            unlink("./assets/uploads/files/" . $old_skpd[0]['pdf_file']);
                        }
                    }
                }
    
                if (!empty($billboard_img)) {
                    $upload2 = $this->img_upload->do_upload("billboard_img");
                    $img = $this->img_upload->data();
                    $skpd_data['billboard_img'] = $img['file_name'];
                    if ($old_skpd[0]['billboard_img']) {
                        if(file_exists("./assets/uploads/images/" . $old_skpd[0]['billboard_img'])){
                            unlink("./assets/uploads/images/" . $old_skpd[0]['billboard_img']);
                        }
                    }
                }
    
                $skpd_data['update_at'] = date('Y-m-d H:i:s');
                $skpd_update = $this->mm->update('skpd', $skpd_data, 'skpd_id', $skpd_id);
                if ($skpd_update) {
                    $skpd_active = $this->mm->maxSkpdPeriod1($skpd_data['atm_id']);
                    if (!$skpd_active) {
                        $skpd_active = $this->mm->maxSkpdPeriod2($skpd_data['atm_id']);
                    }
                    $atm_data['skpd_active'] = $skpd_active;
                    $update_atm = $this->mm->update('atm', $atm_data, 'atm_id', $skpd_data['atm_id']);
                    if ($update_atm) {
                        $atm_name = $this->mm->getOne('atm', 'name', 'atm_id', $skpd_data['atm_id']);
                        logs("Upadte SKPD baru dengan Nomor : {$skpd_data['skpd_number']} pada ATM : {$atm_name}");
                        r_success();
                    }
                }
            }
        }else{
            r_error_data("Periode akhir harus lebih besar daripada periode awal");
        }

       
    }

    public function detailAtm($atm_id)
    {
        $data['atm_id'] = $atm_id;
        $data['atm'] = $this->mm->getArrayWhere('atm', 'atm_id', $atm_id);
        $data['place_installation'] = $this->mm->getOne('skpd', 'place_installation', 'skpd_id', $data['atm'][0]['skpd_active']);
        $this->load->view('content/admin/reklame/atm/detail', $data);
    }

    public function skpdTable($atm_id)
    {
        $id = 'skpd_id';
        $table = 'skpd';
        $param = $atm_id;
        $column = array(
            'skpd_id',
            'skpd_number',
            'start_period',
            'end_period',
            'comment',
            'pdf_file',
            'billboard_img',
            'atm_id',
        );
        showOneTable($id, $table, $column, $param);
    }

    public function getDataAtm()
    {
        $obj = to_json();
        $key_id = $obj->key_id;
        $atm = $this->mm->getAtm($key_id);
        if ($atm) {
            r_success_data($atm);
        }
    }

    public function changeStatus($atm_id)
    {
        $obj = to_json();
        $status = $obj->status;
        if ($status == 'true') {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $this->mm->update('atm', $data, 'atm_id', $atm_id);
        r_success();
    }
}