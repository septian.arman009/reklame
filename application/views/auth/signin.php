<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<!-- <div class="login-logo"></div> -->
	<div class="login-body">
		<div class="login-title">
			<strong class="l-title">Selamat Datang</strong>
		</div>
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input id="email" type="email" class="form-control" placeholder="Email" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<input id="password" type="password" class="form-control" placeholder="Password" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>forgot_password" class="btn btn-link btn-block">Lupa Password ?</a>
				</div>
				<div class="col-md-6">
					<a id="signin" onclick="signinProccess()" class="btn btn-info btn-block">Signin</a>
				</div>
			</div>
		</form>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2019 GO Reklame
		</div>
		<div class="pull-right">
			
		</div>
	</div>
</div>

<!-- failed -->
<button style="display:none" id="failed" type="button" class="btn btn-danger mb-control" data-box="#message-box-sound-2">Fail</button>
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="message-box-sound-2">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-times"></span> Error </div>
			<div class="mb-content">
				<p>Login gagal, silakan cek email & password</p>
			</div>
			<div class="mb-footer">
				<button class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>

<script id="signinjs">
	var input = document.getElementById("email");

	input.addEventListener("keyup", function (event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("signin").click();
		}
	});

	var input1 = document.getElementById("password");

	input1.addEventListener("keyup", function (event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			document.getElementById("signin").click();
		}
	});

	document.getElementById('signinjs').innerHTML = "";
</script>

<style>
	.login-body {
		border: 1px solid #fe970a;
	}

	.l-title {
		color: #fe970a;
	}
</style>

<?php $this->load->view('auth/layout/notif')?>
<?php $this->load->view('auth/layout/footer')?>