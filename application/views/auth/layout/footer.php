</div>

<button id="modalError" style="display:none" type="button" class="btn btn-danger mb-control" data-box="#message-box-sound-2">Fail</button>
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="message-box-sound-2">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-times"></span> Error </div>
			<div class="mb-content">
				<p id="mError"></p>
			</div>
			<div class="mb-footer">
				<button onclick="resetModal()" class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>

<button id="modalSuccess" style="display:none" class="btn btn-default mb-control" data-box="#message-box-success"></button>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-check"></span> Success </div>
			<div class="mb-content">
				<p id="mSuccess"></p>
			</div>
			<div class="mb-footer">
				<button onclick="resetModal()" class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>

</body>

<!-- START PRELOADS -->
<audio id="audio-alert" src="<?php echo base_url() ?>assets/admin/audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="<?php echo base_url() ?>assets/admin/audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS -->

<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins/bootstrap/bootstrap.min.js"></script>

<script type='text/javascript' src='<?php echo base_url() ?>assets/admin/js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins/scrolltotop/scrolltopcontrol.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/plugins.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/js/actions.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/myjs/auth.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/myjs/main.js"></script>

</html>
