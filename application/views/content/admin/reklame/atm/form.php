<ul class="breadcrumb">
	<li>
		<a href="#">Reklame</a>
	</li>
	<li>
		<a href="#">ATM</a>
	</li>
	<?php if($key_id == 'null'){ ?>
	<li class="active">Tambah ATM</li>
	<?php }else{ ?>
	<li class="active">Ubah ATM</li>
	<?php } ?>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">

			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php if($key_id == 'null'){ ?>
						<h3 class="panel-title"><strong>Tambah</strong> ATM</h3>
						<?php }else{ ?>
						<h3 class="panel-title"><strong>Ubah</strong> ATM</h3>
						<?php } ?>
					</div>
					<?php if($key_id == 'null'){ ?>
					<a onclick="loadView('reklame_controller/atm', '.content')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>
					<?php }else{ ?>
					<a onclick="loadView('reklame_controller/detailAtm/<?php echo $key_id ?>', '.content')"
						class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>
					<?php } ?>
					<div class="panel-body">

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">ID ATM</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-desktop"></span></span>
									<input onchange="atmCheck($(this).val())" id="name" type="text"
										class="form-control" />
								</div>
								<span id="atmExist" class="help-block" style="color: red"></span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Nomor SKPD</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-file-text"></span></span>
									<input onchange="skpdCheck($(this).val())" id="skpd_number" type="number"
										class="form-control" />
								</div>
								<span id="skpdExist" class="help-block" style="color: red"></span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Area Koordinasi</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
									<input onkeyup="formCheck()" id="co_area" type="text" class="form-control" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Isi Text Reklame</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-align-center"></span></span>
									<input onkeyup="formCheck()" id="billboard_text" type="text" class="form-control" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Tempat Pemasangan</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
									<input onkeyup="formCheck()" id="place_installation" type="text"
										class="form-control" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Masa Pajak Reklame</label>
							<div class="col-md-2 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
									<input onkeyup="formCheck()" style="color: black; cursor: pointer;"
										id="start_period" type="text" class="form-control mask_date" />
								</div>
							</div>

							<div class="col-md-2">
								<p style="text-align: center">s/d</p>
							</div>

							<div class="col-md-2 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
									<input onkeyup="formCheck()" style="color: black; cursor: pointer;" id="end_period"
										type="text" class="form-control mask_date" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">PDF File (.pdf)</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-file"></span></span>
									<input onchange="formCheck()" id="file_pdf" name="file_pdf" type="file"
										class="form-control" />
									<span class="input-group-addon" id="file_name"></span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Foto Reklame
								(.jpg/.png/.JPG/.JPEG/.pdf)</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><span class="fa fa-picture-o"></span></span>
									<input onchange="formCheck()" id="billboard_img" name="billboard_img" type="file"
										class="form-control" />
									<span class="input-group-addon" id="img_name"></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Comment</label>
							<div class="col-md-6 col-xs-12">
								<textarea id="comment" class="form-control" rows="5"></textarea>
							</div>
						</div>

					</div>
					<div class="panel-footer">
						<button id="reset" onclick="reset()" class="btn btn-default">Bersihkan</button>
						<button disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</button>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script id="formatmjs">
	var key_id = '<?php echo $key_id ?>';
	var skpd_id = '<?php echo $skpd_id ?>';

	$(document).ready(function () {
		$("input.mask_date").mask('99-99-9999');
	});

	getData(key_id);

	function reset() {
		$("#name").val('');
		$("#skpd_number").val('');
		$("#co_area").val('');
		$("#billboard_text").val('');
		$("#place_installation").val('');
		$("#file_pdf").val('');
		$("#billboard_img").val('');
		$("#comment").val('');
		formCheck();
	}

	function formCheck() {
		if (this.v_atm == 'true' && this.v_skpd == 'true') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {

		var formData = new FormData();
		formData.append("key_id", key_id);
		formData.append("skpd_id", skpd_id);
		formData.append("name", $("#name").val());
		formData.append("skpd_number", $("#skpd_number").val());
		formData.append("co_area", $("#co_area").val());
		formData.append("billboard_text", $("#billboard_text").val());
		formData.append("place_installation", $("#place_installation").val());
		formData.append("start_period", $("#start_period").val());
		formData.append("end_period", $("#end_period").val());
		formData.append("comment", $("#comment").val());

		var pdf = $('input[name=file_pdf]');
		var img = $('input[name=billboard_img]');
		var filePdf = pdf[0].files[0];
		var billImg = img[0].files[0];

		if (key_id == 'null') {
			if (filePdf) {
				var ext1 = filePdf['name'].search('.pdf');
				if (ext1 < 0) {
					mError('Silakan masukan extension masing - masing file sesuai ketentuan');
					throw '';
				}
			}

			if (billImg) {
				var ext2 = billImg['name'].search('.jpg');
				var ext3 = billImg['name'].search('.png');
				var ext4 = billImg['name'].search('.JPG');
				var ext5 = billImg['name'].search('.JPEG');
				var ext6 = billImg['name'].search('.pdf');
				if (ext2 > 0 || ext3 > 0 || ext4 > 0 || ext5 > 0 || ext6 > 0) {} else {
					mError('Silakan masukan extension masing - masing file sesuai ketentuan');
					throw '';
				}
			}
			formData.append("file_pdf", filePdf);
			formData.append("billboard_img", billImg);
			if (isValidDate($("#start_period").val()) && isValidDate($("#end_period").val())) {
				doAction(formData, key_id);
			} else {
				mError('Format tanggal tidak sesuai');
			}
		} else {
			if (filePdf) {
				var ext1 = filePdf['name'].search('.pdf');
				if (ext1 < 0) {
					mError('Silakan masukan extension masing - masing file sesuai ketentuan');
					throw '';
				}
			}

			if (billImg) {
				var ext2 = billImg['name'].search('.jpg');
				var ext3 = billImg['name'].search('.png');
				var ext4 = billImg['name'].search('.JPG');
				var ext5 = billImg['name'].search('.JPEG');
				var ext6 = billImg['name'].search('.pdf');
				if (ext2 > 0 || ext3 > 0 || ext4 > 0 || ext5 > 0 || ext6 > 0) {} else {
					mError('Silakan masukan extension masing - masing file sesuai ketentuan');
					throw '';
				}
			}
			formData.append("file_pdf", filePdf);
			formData.append("billboard_img", billImg);
			if (isValidDate($("#start_period").val()) && isValidDate($("#end_period").val())) {
				doAction(formData, key_id);
			} else {
				mError('Format tanggal tidak sesuai');
			}
		}
	}

	function doAction(formData, id) {
		$("#save").html('Loading ..');
		formPost("reklame_controller/action", formData, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					if (id == 'null') {
						$("#save").html('Simpan');
						mSuccess('Simpan data berhasil');
						loadView('reklame_controller/form/atm/null/null', '.content');
					} else {
						mSuccess('Simpan data berhasil');
						loadView('reklame_controller/detailAtm/' + key_id, '.content');
					}	
				} else {
					$("#save").html('Simpan');
					mError(response.data);
				}
			} else {
				console.log('error : ', err);
			}
		});
	}

	function atmCheck(name) {
		if (name != '') {
			var data = {
				key_id: key_id,
				value: name,
				where: 'name',
				column: 'name',
				table: 'atm'
			}

			postData('main_controller/singleCheck', data, function (err, response) {
				if (response) {
					var status = response.status;
					if (status == 'success') {
						$("#atmExist").html('');
						this.v_atm = 'true';
						setTimeout(() => {
							formCheck();
						}, 100);
					} else {
						$("#atmExist").html('ID ATM sudah digunakan');
						this.v_atm = 'false';
						$("#save").attr('disabled', 'disabled');
					}
				} else {
					console.log('error : ', err);
				}
			});
		} else {
			$("#atmExist").html('');
		}
	}

	function skpdCheck(number) {
		if (number != '') {
			var data = {
				key_id: key_id,
				value: number,
				where: 'skpd_number',
				column: 'skpd_number',
				table: 'skpd'
			}

			postData('main_controller/singleCheck', data, function (err, response) {
				if (response) {
					var status = response.status;
					if (status == 'success') {
						$("#skpdExist").html('');
						this.v_skpd = 'true';
						setTimeout(() => {
							formCheck();
						}, 100);
					} else {
						$("#skpdExist").html('Nomor SKPD sudah digunakan');
						this.v_skpd = 'false';
						$("#save").attr('disabled', 'disabled');
					}
				} else {
					console.log('error : ', err);
				}
			});
		} else {
			$("#skpdExist").html('');
		}
	}

	function getData(key_id) {
		if (key_id != 'null') {
			data = {
				key_id: key_id
			}

			postData('reklame_controller/getDataAtm', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#name").val(response.data[0].name);
						$("#skpd_number").val(response.data[0].skpd_number);
						$("#co_area").val(response.data[0].co_area);
						$("#billboard_text").val(response.data[0].billboard_text);
						$("#place_installation").val(response.data[0].place_installation);
						$("#start_period").val(bootstrap_date(response.data[0].start_period));
						$("#end_period").val(bootstrap_date(response.data[0].end_period));
						$("#comment").val(response.data[0].comment);
						
						if(response.data[0].pdf_file != null){
							$("#file_name").html('<a target="_blank" style="text-decoration: none; color: white;" href="assets/uploads/files/'+ response.data[0].pdf_file +'">...'+ response.data[0].pdf_file.slice(20) +'</a>');
						}
						if(response.data[0].billboard_img != null){
							$("#img_name").html('<a target="_blank" style="text-decoration: none; color: white;" href="assets/uploads/images/'+ response.data[0].billboard_img +'">...'+ response.data[0].billboard_img.slice(20) +'</a>');
						}

						this.v_atm = 'true';
						this.v_skpd = 'true';
						$("#clear").attr('disabled', 'disabled');
						setTimeout(() => {
							formCheck();
						}, 100);
					} else {
						this.v_atm == 'false';
						this.v_skpd == 'false';
						setTimeout(() => {
							formCheck();
						}, 100);
					}
				} else {
					console.log('error : ', err);
				}
			});
		}
	}

	function isValidDate(dateString) {
		// First check for the pattern
		// if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
		// 	return false;

		// Parse the date parts to integers
		var parts = dateString.split("-");
		var day = parseInt(parts[0], 10);
		var month = parseInt(parts[1], 10);
		var year = parseInt(parts[2], 10);

		// Check the ranges of month and year
		if (year < 1000 || year > 3000 || month == 0 || month > 12)
			return false;

		var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

		// Adjust for leap years
		if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
			monthLength[1] = 29;

		// Check the range of the day
		return day > 0 && day <= monthLength[month - 1];
	};

	document.getElementById('formatmjs').innerHTML = "";
</script>