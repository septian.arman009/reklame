<body>
	<!-- START PAGE CONTAINER -->
	<div class="page-container page-navigation-top-fixed">

		<!-- START PAGE SIDEBAR -->
		<div class="page-sidebar page-sidebar-fixed scroll">
			<!-- START X-NAVIGATION -->
			<ul class="x-navigation">
				<li class="xn-logo">
					<a>SKPD</a>
					<a class="x-navigation-control"></a>
				</li>

				<li class="xn-profile">
					<a class="profile-mini">
						<img src="<?php echo base_url() ?>assets/admin/img/reklame-1.png" alt="John Doe" />
					</a>
					<div class="profile">
						<div class="profile-image">
							<img src="<?php echo base_url() ?>assets/admin/img/reklame-1.png" alt="John Doe" />
						</div>
						<div class="profile-data">
							<div class="profile-data-name">
								<?php echo $_SESSION['rek_in']['name'] ?>
							</div>
							<div class="profile-data-title">
								<?php echo $_SESSION['rek_in']['role_name'] ?>
							</div>
						</div>
						<div class="profile-controls">
							<a onclick="sub_menu('#master_data','#users','master_controller/form/users/<?php echo whoIAM()['id'] ?>/null','.content')" class="profile-control-left">
								<span class="fa fa-user"></span>
							</a>
							<a onclick="sub_menu('#settings','#changePassword','main_controller/changePassword')" class="profile-control-right">
								<span class="fa fa-lock"></span>
							</a>
						</div>
					</div>
				</li>

				<li class="xn-title">Navigasi Utama</li>
				<li id="home">
					<a onmousedown="spinner('#home_spin', '#home_normal')"
						onclick="main_menu('#home','main_controller/home/1')">
						<span class="fa fa-home" id="home_normal"></span>
						<span class="fa fa-refresh fa-spin" style="display: none;" id="home_spin"></span>
						<span class="xn-text">Halaman Utama</span>
					</a>
				</li>
				
				<li id="master_data" class="xn-openable">
					<a>
						<span class="fa fa-hdd-o"></span>
						<span class="xn-text">Data Master</span>
					</a>
					<ul>
						<li id="users">
							<a onmousedown="spinner('#users_spin','#users_normal')"
								onclick="sub_menu('#master_data','#users','master_controller/user')">
								<span class="fa fa-user" id="users_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="users_spin"></span>
								Users
							</a>
						</li>
					</ul>
				</li>

				<li id="reklame" class="xn-openable">
					<a>
						<span class="fa fa-bullhorn"></span>
						<span class="xn-text">Reklame</span>
					</a>
					<ul>
						<li id="atm">
							<a onmousedown="spinner('#atm_spin','#atm_normal')"
								onclick="sub_menu('#reklame','#atm','reklame_controller/atm')">
								<span class="fa fa-desktop" id="atm_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="atm_spin"></span>
								ATM
							</a>
						</li>
					</ul>
				</li>

				<li id="settings" class="xn-openable">
					<a>
						<span class="fa fa-gear"></span>
						<span class="xn-text">Pengaturan</span>
					</a>
					<ul>
					<?php if(role(['admin'], false)){ ?>
						<li id="emailSystem">
							<a onmousedown="spinner('#emailSystem_spin','#emailSystem_normal')"
								onclick="sub_menu('#settings','#emailSystem','main_controller/emailSystem')">
								<span class="fa fa-envelope" id="emailSystem_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;"
									id="emailSystem_spin"></span>
								Sistem Email
							</a>
						</li>
					<?php } ?>
						<li id="changePassword">
							<a onmousedown="spinner('#changePassword_spin','#changePassword_normal')"
								onclick="sub_menu('#settings','#changePassword','main_controller/changePassword')">
								<span class="fa fa-lock" id="changePassword_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;"
									id="changePassword_spin"></span>
								Ganti Password
							</a>
						</li>
					</ul>
				</li>
			</ul>
			<!-- END X-NAVIGATION -->
		</div>
		<!-- END PAGE SIDEBAR -->
	