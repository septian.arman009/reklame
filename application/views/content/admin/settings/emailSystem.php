<ul class="breadcrumb">
	<li>
		<a href="#">Pengaturan</a>
	</li>
	<li class="active">Sistem Email</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Pengaturan</strong> Email</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Email</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-envelope"></span>
									</span>
									<input onkeyup="check()" id="send_mail" autocomplete="new-email" type="text"
										class="form-control" value="<?php echo $setting[0]['send_mail'] ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Password</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-lock"></span>
									</span>
									<input onkeyup="check()" id="send_pass" autocomplete="new-password" type="password"
										class="form-control" value="<?php echo $setting[0]['send_pass'] ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Protocol</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-gear"></span>
									</span>
									<input onkeyup="check()" id="protocol" type="text" class="form-control"
										value="<?php echo $setting[0]['protocol'] ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Mail Host</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-gear"></span>
									</span>
									<input onkeyup="check()" id="mail_host" type="text" class="form-control"
										value="<?php echo $setting[0]['mail_host'] ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Mail Port</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-gear"></span>
									</span>
									<input onkeyup="check()" id="mail_port" type="number" class="form-control"
										value="<?php echo $setting[0]['mail_port'] ?>">
								</div>
							</div>
						</div>
					</div>

					<div class="panel-footer">
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Save</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<script id="emailsystemjs">
	$(document).ready(function () {
		$("#emailSystem_spin").hide();
		$("#emailSystem_normal").show();
		check();
	});

	function check() {
		var send_mail = $("#send_mail").val();
		var send_pass = $("#send_pass").val();
		var protocol = $("#protocol").val();
		var mail_host = $("#mail_host").val();
		var mail_port = $("#mail_port").val();
		if (send_mail != '' && send_pass != '' && protocol != '' && mail_host != '' && mail_port != '') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").html('Loading ..');
		var data = {
			send_mail: $('#send_mail').val(),
			send_pass: $('#send_pass').val(),
			protocol: $('#protocol').val(),
			mail_host: $('#mail_host').val(),
			mail_port: $('#mail_port').val()
		}


		postData('main_controller/updateEmailSystem', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").html('Simpan');
					mSuccess('Update email sistem berhasil');
					sub_menu('#settings', '#emailSystem', 'main_controller/emailSystem');
				} else {
					$("#save").html('Simpan');
					mError('Gagal update email sistem');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('emailsystemjs').innerHTML = "";
</script>