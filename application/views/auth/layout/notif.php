<button id="btn-s" style="display:none" class="btn btn-default mb-control" data-box="#message-box-success"></button>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-check"></span> Success </div>
			<div class="mb-content">
				<p id="message-btn-s"></p>
			</div>
			<div class="mb-footer">
				<button onclick="resetModal()" class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>

<button id="btn-e" style="display:none" class="btn btn-default mb-control" data-box="#message-box-warning"></button>
<div class="message-box message-box-danger animated fadeIn" data-sound="fail" id="message-box-warning">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-times"></span> Error </div>
			<div class="mb-content">
				<p id="message-btn-e"></p>
			</div>
			<div class="mb-footer">
				<button onclick="resetModal()" class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
			</div>
		</div>
	</div>
</div>