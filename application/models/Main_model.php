<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_model extends CI_Model
{

    public function getArray($table)
    {
        return $this->db->get($table)->result_array();
    }

    public function getArrayWhere($table, $where, $key)
    {
        $this->db->where($where, $key);
        return $this->db->get($table)->result_array();
    }

    public function getArray2Where(
        $table, 
        $where_1, $key_1, 
        $where_2, $key_2)
    {
        $this->db->where($where_1, $key_1);
        $this->db->where($where_2, $key_2);
        return $this->db->get($table)->result_array();
    }

    public function getArray3Where(
        $table, 
        $where_1, $key_1, 
        $where_2, $key_2,
        $where_3, $key_3)
    {
        $this->db->where($where_1, $key_1);
        $this->db->where($where_2, $key_2);
        $this->db->where($where_3, $key_3);
        return $this->db->get($table)->result_array();
    }

    function getWhereIn($table, $where, $data)
    {
        $this->db->where_in($where, $data);
        return $this->db->get($table)->result_array();
    }

    public function getArrayLike($table, $column, $key)
    {
        $this->db->like($column, $key, 'both');
        return $this->db->get($table)->result_array();
    }

    public function getOne($table, $get, $where, $key)
    {
        $this->db->where($where, $key);
        $query = $this->db->get($table)->result_array();
        if ($query) {
            if ($query[0][$get]) {
                return $query[0][$get];
            } else {
                return null;
            }
        }
    }

    public function getArrayLimitOrder($table, $start, $end, $order, $sort)
    {
        $this->db->limit($end, $start);
        $this->db->order_by($order, $sort);
        return $this->db->get($table)->result_array();
    }

    public function countRow($table)
    {
        return $this->db->get($table)->num_rows();
    }

    public function insert($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function insertGetId($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function getOneInt($table, $get, $where, $key)
    {
        $this->db->where($where, $key);
        $query = $this->db->get($table)->result_array();
        if ($query) {
            if ($query[0][$get]) {
                return $query[0][$get];
            } else {
                return 0;
            }
        }
    }

    public function update($table, $data, $where, $key)
    {
        $this->db->where($where, $key);
        return $this->db->update($table, $data);
    }

    public function updateWhere(
        $table, 
        $data, 
        $where_1, $key_1, 
        $where_2, $key_2)
    {
        $this->db->where($where_1, $key_1);
        $this->db->where($where_2, $key_2);
        return $this->db->update($table, $data);
    }

    public function update2Where(
        $table, 
        $data, 
        $where_1, $key_1, 
        $where_2, $key_2,
        $where_3, $key_3)
    {
        $this->db->where($where_1, $key_1);
        $this->db->where($where_2, $key_2);
        $this->db->where($where_3, $key_3);
        return $this->db->update($table, $data);
    }

    public function updateMultiple($table, $data, $where)
    {
        return $this->db->update_batch($table, $data, $where);
    }

    public function insertMultiple($table, $data)
    {
        return $this->db->insert_batch($table, $data);
    }

    public function destroy($table, $where, $key)
    {
        $this->db->where($where, $key);
        return $this->db->delete($table);
    }

    public function lastAi($table)
    {
        $query = $this->db->query("SHOW TABLE STATUS LIKE '$table'")->result_array();
        return $query[0]['Auto_increment'];
    }

    public function maxInColumn($table, $column)
    {
        $this->db->select_max($column);
        $result = $this->db->get($table)->row();  
        return $result->$column;
    }

    public function minInColumn($table, $column)
    {
        $this->db->select_min($column);
        $result = $this->db->get($table)->row();  
        return $result->$column;
    }

    public function getAtm($atm_id)
    {  
        return $this->db->query("select a.name, b.* from atm a, skpd b where a.atm_id = {$atm_id} and a.skpd_active = b.skpd_id")->result_array();
    }

    public function maxSkpdPeriod1($atm_id)
    {
        $result = $this->db->query("select skpd_id from skpd where end_period = (select max(end_period) from skpd where atm_id = $atm_id) and update_at = (select max(update_at) from skpd) and atm_id = $atm_id")->row();  
        if($result){
            return $result->skpd_id;
        }else{
            return false;
        }
    }

    public function maxSkpdPeriod2($atm_id)
    {
        $result = $this->db->query("select skpd_id from skpd where end_period = (select max(end_period) from skpd where atm_id = $atm_id) and atm_id = $atm_id")->row();  
        if($result){
            return $result->skpd_id;
        }else{
            return false;
        }
    }

}

// select * from atm where name like '%011805180802011296%'
// select skpd_id from skpd where end_period = (select max(end_period) from skpd where atm_id = 269) and atm_id = 269
