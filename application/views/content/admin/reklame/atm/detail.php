<ul class="breadcrumb">
	<li>
		<a href="#">Reklame</a>
	</li>
	<li class="active">Detail ATM</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Detail ATM</h3>
				</div>
				<a onclick="sub_menu('#reklame','#atm','reklame_controller/atm')" class="btn btn-default">
					<i class="fa fa-arrow-left"></i>
				</a>
				<div class="panel-body">
					<button onclick="loadView('reklame_controller/form/skpd/<?php echo $atm_id ?>/null', '.content')"
						class="btn btn-default mb-control" data-box="#message-box-sound-2">Tambah SKPD Baru</button>
					<button
						onclick="loadView('reklame_controller/form/atm/<?php echo $atm_id ?>/<?php echo $atm[0]['skpd_active'] ?>', '.content')"
						class="btn btn-info mb-control" data-box="#message-box-sound-2">Ubah</button>
					<button onclick="destroyAtm('<?php echo $atm_id ?>')" class="btn btn-danger mb-control"
						data-box="#message-box-sound-2">Hapus</button>
				</div>
				<div class="panel-body form-group-separated">
					<div class="form-group">
						<label class="col-md-3 col-xs-3 control-label"></label>
						<div class="col-md-6 col-xs-6">
							<h3><?php echo $atm[0]['name'] ?></h3>
						</div>
					</div>
				</div>
				<div class="panel-body form-group-separated">
					<div class="form-group">
						<label class="col-md-3 col-xs-3 control-label"></label>
						<div class="col-md-6 col-xs-6">
							<h3><?php echo $place_installation ?></h3>
						</div>
					</div>
				</div>
				<div class="panel-body form-group-separated">
					<div class="form-group">
						<label class="col-md-3 col-xs-3 control-label"></label>
						<div class="col-md-6 col-xs-6">
							<label class="switch switch-small">
								<?php if($atm[0]['status'] == 1){ ?>
									<input onchange="changeStatus()" id="changeStatus" checked type="checkbox">
									<span></span>
								<?php }else{ ?>
									<input onchange="changeStatus()" id="changeStatus" type="checkbox">
									<span></span>
								<?php } ?>
							</label>
						</div>
					</div>
				</div>
				<div class="panel-body form-group-separated">
				</div>
				<div class="panel-body">
					<table id="skpd-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Nomor SKPD</th>
								<th id="th">Masa Berlaku Awal</th>
								<th id="th">Masa Berlaku Akhir</th>
								<th id="th">Comment</th>
								<th id="th" width="15%">PDF File</th>
								<th id="th" width="15%">Foto Reklame</th>
								<th id="th" class="no-sort" width="10%">Customize</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">Nomor SKPD</th>
								<th class="footer">DD-MM-YYYY</th>
								<th class="footer">DD-MM-YYYY</th>
								<th class="footer">Comment</th>
							</tr>
						</tfoot>
					</table>
				</div>
				<div class="panel-footer">
					<button onclick="reset()" class="btn btn-default">Bersihkan Pencarian Tanggal</button>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>

<script id="skpdjs">
	$('#skpd-table tfoot th').each(function () {
		var title = $(this).text();
		if (title == 'DD-MM-YYYY') {
			var inp =
				'<input readonly style="cursor: pointer; color: black;" type="text" class="form-control footer-s datepicker" placeholder="' +
				title + '" id="' +
				title + ' data-date="<?php echo date("Y-m-d") ?>" />';
		} else {
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" id="' + title +
				'" />';
		}
		$(this).html(inp);
	});

	var table = $('#skpd-table').DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": 'reklame_controller/skpdTable/<?php echo $atm_id ?>',
			"type": "POST"
		}
	});

	table.columns().every(function () {
		var that = this;
		$('input', this.footer()).on('keyup change', function () {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		});
	});

	$(".datepicker").datepicker({
		format: 'dd-mm-yyyy'
	});

	$("#no").hide();
	$("#status").hide();

	function edit(skpd_id) {
		loadView('reklame_controller/form/skpd/<?php echo $atm_id ?>/' + skpd_id, '.content');
	}

	function destroy(id) {
		mConfirm('Anda yakin ingin mmenghapus data SKPD dengan ID :  ' + id +
			' ?, Data akan dihapus secara permanen dan tidak bisa dikembalikan', "do_destroy('" + id + "')");
	}

	function do_destroy(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/skpd/skpd_id', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					loadView('reklame_controller/detailAtm/<?php echo $atm_id ?>', '.content');
					mSuccess('SKPD berhasil dihapus');
				} else {
					mError('Gagal hapus SKPD');
				}
			} else {
				console.log('error : ', err);
			}
		});
	}

	function destroyAtm(id) {
		mConfirm('Anda yakin ingin mmenghapus data ATM dengan ID :  ' + id +
			' ?, Data akan dihapus secara permanen dan tidak bisa dikembalikan', "do_destroyAtm('" + id + "')");
	}

	function do_destroyAtm(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/atm/atm_id', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					loadView('reklame_controller/atm', '.content');
					mSuccess('ATM berhasil dihapus');
				} else {
					mError('Gagal hapus ATM');
				}
			} else {
				console.log('error : ', err);
			}
		});
	}

	function reset() {
		loadView('reklame_controller/detailAtm/<?php echo $atm_id ?>', '.content');
	}
	
	function changeStatus(){
		if (_("changeStatus").checked) {
			var status = 'false';
		} else {
			var status = 'true';
		}

		var data = {
			status: status
		}
		
		postData('reklame_controller/changeStatus/<?php echo $atm_id ?>', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					mSuccess('Status ATM berhasil disimpan');
					loadView('reklame_controller/detailAtm/<?php echo $atm_id ?>', '.content');
				} else {
					mError('Gagal menyimpan Status ATM');
				}
			} else {
				console.log('error : ', err);
			}
		});
	}
	document.getElementById('skpdjs').innerHTML = "";
</script>

<style>
	#skpd-table_filter {
		display: none;
	}
</style>