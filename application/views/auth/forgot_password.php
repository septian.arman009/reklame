<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<!-- <div class="login-logo"></div> -->
	<div class="login-body">
		<div class="login-title">
			<strong class="l-title">Masukan Email</strong>
		</div>
		<div class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="emailFormat()" id="email" type="email" class="form-control" placeholder="Email" />
					<p id="invalid_email" class="btn btn-link btn-block" style="display:none;color:white;">Silakan masukan format email yang benar</p>
					<p id="notexist" class="btn btn-link btn-block" style="display:none;color:white;">Email tidak ditemukan</p>
				</div>

			</div>
			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>signin" class="btn btn-link btn-block">Kembali Ke Sign</a>
				</div>
				<div class="col-md-6">
					<a disabled id="send_token" onclick="send_token()" class="btn btn-info btn-block">Kirim Link</a>
				</div>
			</div>
		</div>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2019 GO Reklame
		</div>
		<div class="pull-right">
		</div>
	</div>
</div>

<style>
	.login-body {
		border: 1px solid #fe970a;
	}

	.l-title {
		color: #fe970a;
	}
</style>

<?php $this->load->view('auth/layout/notif')?>
<?php $this->load->view('auth/layout/footer')?>