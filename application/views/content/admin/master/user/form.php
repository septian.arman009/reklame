<ul class="breadcrumb">
	<li>
		<a href="#">Master Data</a>
	</li>
	<li>Users</li>
	<?php if ($key_id == 'null') {?>
	<li class="active">Tambah User</li>
	<?php }else{?>
	<li class="active">Edit User</li>
	<?php }?>

</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<?php if ($key_id == 'null') {?>
						<h3 class="panel-title">
							<strong>Tambah</strong> User</h3>
						<?php }else{?>
						<h3 class="panel-title">
							<strong>Edit</strong> User</h3>
						<?php }?>
					</div>
					<?php if(role(['admin'], false)){ ?>
					<a onclick="loadView('master_controller/user', '.content')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>
					<?php }else{ ?>
					<a onclick="main_menu('#home','main_controller/home/1')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>
					<?php } ?>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label">Nama</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-user"></span>
											</span>
											<input onkeyup="formCheck()" id="name" type="text" class="form-control">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Email</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-envelope-o"></span>
											</span>
											<input autocomplete="new-email" onkeyup="emailCheck($(this).val())" id="email" type="email"	class="form-control">
										</div>
										<span class="help-block" style="display:none;color:red" id="invalid_email">Silakan masukan format email yang benar</span>
										<span class="help-block" style="display:none;color:red" id="registered">Email sudah digunakan</span>
									</div>
								</div>

								<?php if($key_id == 'null'){ ?>
								<div class="form-group">
									<label class="col-md-3 control-label">Password</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-lock"></span>
											</span>
											<input autocomplete="new-password" onkeyup="passwordCheck()" id="password" type="password" class="form-control">
										</div>
										<p style="display:none;color:red;" id="passless">Minimal 8 karakter</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Konfirmasi Password</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-lock"></span>
											</span>
											<input onkeyup="passwordCheck()" id="confirm" type="password" class="form-control">
										</div>
										<div style="display:none;color:red" id="conless">Minimal 8 karakter</div>
										<div style="display:none;color:red" id="notmatch">Password tidak sama</div>
									</div>
								</div>

								<?php } ?>

								<div class="form-group">
									<label class="col-md-3 control-label">Level</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-key"></span>
											</span>
											<select onchange="formCheck()" class="form-control" id="role_id">
												<?php foreach ($roles as $key => $value) { ?>
												<?php if(role(['admin'], false)){ ?>
												<option value="<?php echo $value['role_id'] ?>">
													<?php echo $value['display_name'] ?>
												</option>
												<?php } else {?>
												<?php if($value['role_id'] != 1 ) { ?>
												<option value="<?php echo $value['role_id'] ?>">
													<?php echo $value['display_name'] ?>
												</option>
												<?php } ?>
												<?php } ?>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a id="clear" onclick="reset()" class="btn btn-default">Bersihkan</a>
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Simpan</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="formuserjs">
	var key_id = '<?php echo $key_id ?>';

	getData(key_id);

	function resetForm() {
		$("#name").val('');
		$("#email").val('');
		$("#password").val('');
		$("#confirm").val('');
		this.v_email = '';
		formCheck();
	}

	function formCheck() {
		var name = $("#name").val();
		var address = $("#address").val();
		var phone = $("#phone").val();
		if (name != '' && address != '' && phone != '' && this.v_email == 'true' && this.passCheck == 'true') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function emailCheck(email) {
		if (email != '') {
			if (ValidateEmail(email, "#save", "#invalid_email")) {
				var data = {
					key_id: key_id,
					value: $("#email").val(),
					where: 'email',
					column: 'user_id',
					table: 'users'
				}

				postData('main_controller/singleCheck', data, function (err, response) {
					if (response) {
						var status = response.status;
						if (status == 'success') {
							$("#invalid_email").hide();
							$("#registered").hide();
							this.v_email = 'true';
							setTimeout(() => {
								formCheck();
							}, 100);
						} else {
							$("#invalid_email").hide();
							$("#registered").show();
							this.v_email = 'false';
							$("#save").attr('disabled', 'disabled');
						}
					} else {
						console.log('error : ', err);
					}
				});
			}
		} else {
			$("#invalid_email").hide();
			$("#registered").hide();
			this.v_email = '';
			$("#save").attr('disabled', 'disabled');
		}
	}

	function passwordCheck() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();

		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen > 0 && passlen < 8) {
			$("#passless").show();
		} else {
			$("#passless").hide();
		}

		if (conlen > 0 && conlen < 8) {
			$("#conless").show();
			$("#notmatch").hide();
		} else {
			$("#conless").hide();
			$("#notmatch").hide();
			if (password.length >= 8 && confirm.length >= 8) {
				if (password == confirm) {
					this.passCheck = 'true';
					$("#notmatch").hide();
					$("#save").removeAttr('disabled');
					setTimeout(() => {
						formCheck();
					}, 100);
				} else {
					this.passCheck = 'false';
					$("#notmatch").show();
					setTimeout(() => {
						formCheck();
					}, 100);
				}
			} else {
				this.passCheck = 'false';
				setTimeout(() => {
					formCheck();
				}, 100);
			}
		}
	}

	function action() {
		$("#save").html('Loading ..');
		var data = {
			key_id: key_id,
			table: 'users',
			name: $('#name').val(),
			email: $('#email').val(),
			password: $('#password').val(),
			role_id: $('#role_id').val()
		}

		postData('master_controller/userAction', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					$("#save").html('Simpan');
					mSuccess('Simpan user berhasil');
					if (key_id == 'null') {
						resetForm();
					}
				} else {
					$("#save").html('Simpan');
					mError('Gagal menyimpan user');
				}
			} else {
				console.log('error : ', err);
			}
		});
	}

	function getData(key_id) {
		if (key_id != 'null') {
			data = {
				key_id: key_id,
				where: 'user_id',
				table: 'users'
			}

			postData('main_controller/getData', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#name").val(response.data[0].name);
						$("#email").val(response.data[0].email);
						$("#role").val(response.data[0].role);
						$("#clear").attr('disabled', 'disabled');
						this.v_email = 'true';
						this.passCheck = 'true';
						setTimeout(() => {
							formCheck();
						}, 100);
					}
				} else {
					console.log('error : ', err);
				}
			});
		}
	}

	document.getElementById('formuserjs').innerHTML = "";
</script>