<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<!-- <div class="login-logo"></div> -->
	<div class="login-body">
		<div class="login-title">
			<strong class="l-title">Buat Password Baru</strong>
		</div>
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="checkPassword()" id="password" type="password" class="form-control"
						placeholder="Password" />
					<p id="passless" class="btn btn-link btn-block" style="display:none;color:white;">Minimal 8 karakter</p>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="checkPassword()" id="confirm" type="password" class="form-control"
						placeholder="Confirm Password" />
					<p id="conless" class="btn btn-link btn-block" style="display:none;color:white;">Minimal 8 karakter</p>
					<p id="notmatch" class="btn btn-link btn-block" style="display:none;color:white;">Password tidak sama</p>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>signin" class="btn btn-link btn-block">Kembali Ke Signin</a>
				</div>
				<div class="col-md-6">
					<a disabled id="reset" onclick="resetPassword()" class="btn btn-info btn-block">Reset</a>
				</div>
			</div>

		</form>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2019 GO Reklame
		</div>
		<div class="pull-right">

		</div>
	</div>
</div>

<script>
	function checkPassword() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();

		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen != '') {
			if (passlen < 8) {
				$("#passless").show();
			} else {
				$("#passless").hide();
			}
		} else {
			$("#passless").hide();
		}

		if (confirm != '') {
			if (conlen < 8) {
				$("#conless").show();
			} else {
				$("#conless").hide();
			}
		} else {
			$("#conless").hide();
		}

		if (password != '' && confirm != '') {
			if (password == confirm) {
				$("#notmatch").hide();

				if (password.length >= 8 && confirm.length >= 8) {
					$("#reset").removeAttr('disabled');
				} else {
					$("#reset").attr('disabled', 'disabled');
				}
			} else {
				$("#notmatch").show();
				$("#reset").attr('disabled', 'disabled');
			}
		} else {
			$("#reset").attr('disabled', 'disabled');
		}
	}

	function resetPassword() {
		$("#reset").html('Loading ..');
		var data = {
			email: '<?php echo $email ?>',
			password: $("#password").val()
		};

		postData("<?php echo base_url() ?>auth_controller/resetPassword", data, function (err, response) {
			if (response) {
				if (response.status == "success") {
					mSuccess('Password berhasil disimpan');
					$("#reset").html('Reset');
					setTimeout(() => {
						window.location = '<?php echo base_url() ?>signin';
					}, 1000);
				} else {
					mError('Gagal menyimpan password');
				}
			}
		});
	}
</script>

<style>
	.login-body {
		border: 1px solid #fe970a;
	}

	.l-title {
		color: #fe970a;
	}
</style>

<?php $this->load->view('auth/layout/notif')?>
<?php $this->load->view('auth/layout/footer')?>