<?php
defined('BASEPATH') or exit('No direct script access allowed');

class D_table extends CI_Model
{

    public function oneTable($dt)
    {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $sql = "SELECT {$columns} FROM {$dt['table']} WHERE atm_id = {$dt['param']}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';

        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                if($dt['table'] == 'skpd' && $i == 1 || $dt['table'] == 'skpd' && $i == 2){
                    $searchCol = to_date_mysql($searchCol);
                }
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }

        if ($where != '') {
            $sql .= " AND " . $where;
        }

        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        $no = 1;
        foreach ($list->result_array() as $row => $val) {
            $rows = array();
            $id = 0;
            $data = 1;
            foreach ($dt['col-display'] as $key => $kolom) {

                if ($id == 0) {
                    $id = $val[$kolom];
                    $rows[] = $no;
                } else if ($dt['table'] == 'skpd' && $data == 3 || $dt['table'] == 'skpd' && $data == 4) {
                    $rows[] = to_date_bootstrap($val[$kolom]);
                } else if ($dt['table'] == 'skpd' && $data == 6) {
                    $rows[] = '<a target="_blank" href="'.base_url("assets/uploads/files/".$val[$kolom]).'">...'.substr($val[$kolom], 21);
                } else if ($dt['table'] == 'skpd' && $data == 7) {
                    $rows[] = '<a target="_blank" href="'.base_url("assets/uploads/images/".$val[$kolom]).'">...'.substr($val[$kolom], 21);
                } else if ($dt['table'] == 'skpd' && $data == 8) {
                    $atm_id = $val[$kolom];
                } else {
                    $rows[] = $val[$kolom];
                }
    
                $data++;
            }
            
            if($dt['table'] == 'skpd'){
                $check = $this->db->query("select * from atm where skpd_active = {$id} and atm_id = {$atm_id}")->result();
                if(!$check){
                    $rows[] =
                        '<a id="edit" title="Edit" class="btn btn-info btn-xs waves-effect"
                        onclick="edit(' . "'" . $id . "'" . ');""><i class="fa fa-pencil-square-o"></i></a>
                        | <a id="destroy" title="Hapus" class="btn btn-danger btn-xs waves-effect"
                        onclick="destroy(' . "'" . $id . "'" . ');"><i class="fa fa-trash-o"></i></a>';
                }else{
                    $rows[] = '-';
                }
            }else{
                $rows[] =
                    '<a id="edit" title="Edit" class="btn btn-info btn-xs waves-effect"
                    onclick="edit(' . "'" . $id . "'" . ');""><i class="fa fa-pencil-square-o"></i></a>
                    | <a id="destroy" title="Hapus" class="btn btn-danger btn-xs waves-effect"
                    onclick="destroy(' . "'" . $id . "'" . ');"><i class="fa fa-trash-o"></i></a>';
            }
           
            $no++;
            $option['data'][] = $rows;

        }
        echo json_encode($option);
    }

    public function twoTable($dt)
    {
        $columns = implode(', ', $dt['col-display']);
        if($dt['table1'] == 'atm a'){
            $sql = "SELECT {$columns} FROM {$dt['table1']}, {$dt['table2']} WHERE {$dt['reference']} = {$dt['foreign']} AND b.skpd_id = a.skpd_active";
        }else{
            $sql = "SELECT {$columns} FROM {$dt['table1']}, {$dt['table2']} WHERE {$dt['reference']} = {$dt['foreign']}";
        }
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';

        for ($i = 0; $i < $count_c; $i++) {
           
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                if($dt['table1'] == 'atm a' && $i == 3){
                    if($i == 3){
                        $where = 'month('. $columnd[$i].') = ' . $searchCol;
                    }else{
                        $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%"';
                    }
                }else {
                    $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                }
                break;
            }
        }

        if ($where != '') {
            $sql .= " AND " . $where;
        }

        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        $no = 1;
        foreach ($list->result_array() as $row => $val) {
            $rows = array();
            $id = 0;
            $data = 1;
            foreach ($dt['col-original'] as $key => $kolom) {

                if ($id == 0) {
                    $id = $val[$kolom];
                    $rows[] = $no;
                } else if ($dt['table1'] == 'atm a' && $data == 2) {
                    $rows[] =  '<a style="font-weight: bold; text-decoration: none;" onclick="detailAtm(' . "'" . $id . "'" . ');">'.$val[$kolom].'</a>';
                } else if ($dt['table1'] == 'atm a' && $data == 4) {
                    $rows[] = to_date_bootstrap($val[$kolom]);
                } else if ($dt['table1'] == 'atm a' && $data == 5) {
                    $status = $val[$kolom];
                    if($status == 1){
                        $rows[] = '<div style="text-align: center; color: limegreen; font-size: 20px;"><i class="fa fa-circle"></i></div>';
                    }else{
                        $rows[] = '<div style="text-align: center; color: red; font-size: 20px;"><i class="fa fa-circle"></i></div>';
                    }
                } else {
                    $rows[] = $val[$kolom];
                }
                
                $data++;
            }

            if($dt['table1'] == 'users a'){
                $rows[] =
                    '<a id="edit" title="Edit" class="btn btn-info btn-xs waves-effect"
                    onclick="edit(' . "'" . $id . "'" . ');"><i class="fa fa-pencil-square-o"></i></a>
                    | <a id="destroy" title="Hapus" class="btn btn-danger btn-xs waves-effect"
                    onclick="destroy(' . "'" . $id . "'" . ');"><i class="fa fa-trash-o"></i></a>';
            }
        
            $no++;
            $option['data'][] = $rows;

        }
        echo json_encode($option);
    }

    public function getOne($table, $take, $column, $id)
    {
        $this->db->where($column, $id);
        $query = $this->db->get($table)->result_array();
        if ($query) {
            if ($query[0][$take]) {
                return $query[0][$take];
            }
        }
    }

}
